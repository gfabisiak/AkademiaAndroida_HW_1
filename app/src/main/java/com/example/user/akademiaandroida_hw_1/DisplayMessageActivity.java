package com.example.user.akademiaandroida_hw_1;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        Player winner = (Player) intent.getSerializableExtra("Winner");


        ConstraintLayout msg_window_cl = findViewById(R.id.MessageWindow);
        if (winner == Player.topPlayer)
            msg_window_cl.setBackgroundColor(getResources().getColor(R.color.topPlayerColor));
        else if (winner == Player.bottomPlayer)
            msg_window_cl.setBackgroundColor(getResources().getColor(R.color.bottomPlayerColor));


        TextView textView = findViewById(R.id.textView);
        textView.setText(message);

        Button playAgainButton = findViewById(R.id.playAgainButton);
        playAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent new_intent = new Intent(DisplayMessageActivity.this, MainActivity.class);
                //new_intent.putExtra("SHOULD_WE_RESET", true);
                startActivity(new_intent);
                DisplayMessageActivity.this.finish();
            }
        });

    }
}
