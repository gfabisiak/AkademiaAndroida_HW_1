package com.example.user.akademiaandroida_hw_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    static int SCORE;
    final static int GOAL = 20;
    public static final String EXTRA_MESSAGE = "com.example.user.akademiaandroida_hw_1.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            SCORE = savedInstanceState.getInt("SCORE");
            updateScoreTextView();
        }
        else {
            SCORE = 0;
        }

        ImageButton imgbtnTopPlayer, imgbtnBottomPlayer;

        imgbtnTopPlayer = findViewById(R.id.imageButtonTopPlayer);
        imgbtnBottomPlayer = findViewById(R.id.imageButtonBottomPlayer);

        TextView score_tv = findViewById(R.id.MiddleOfTheScreenScore);
        score_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetScore();
            }
        });

        imgbtnTopPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SCORE++;
                updateScoreTextView();
                checkVictory();
            }
        });

        imgbtnBottomPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SCORE--;
                updateScoreTextView();
                checkVictory();
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle currentInstanceState) {
        super.onSaveInstanceState(currentInstanceState);
        currentInstanceState.putInt("SCORE", SCORE);
    }

    void checkVictory() {
        if (SCORE == GOAL) // top player won
        {
            Intent intent = new Intent(this, DisplayMessageActivity.class);

            intent.putExtra("Winner", Player.topPlayer);

            String message = getString(R.string.TopPlayerWon);
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
            this.finish();

        }
        else if (SCORE == -GOAL) // bottom player won
        {
            Intent intent = new Intent(this, DisplayMessageActivity.class);

            intent.putExtra("Winner", Player.bottomPlayer);

            String message = getString(R.string.BottomPlayerWon);
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
            this.finish();
        }
    }

    private void resetScore() {
        SCORE= 0;
        updateScoreTextView();
    }

    void updateScoreTextView() {
        TextView score_tv = findViewById(R.id.MiddleOfTheScreenScore);
        if (SCORE > 0) {
            score_tv.setBackgroundColor(getResources().getColor(R.color.topPlayerColor));
            String msg = String.valueOf(SCORE);
            score_tv.setText(msg);
        }
        else if (SCORE < 0) {
            score_tv.setBackgroundColor(getResources().getColor(R.color.bottomPlayerColor));
            String msg = String.valueOf(-SCORE);
            score_tv.setText(msg);
        }
        else { // if SCORE is 0
            score_tv.setBackgroundColor(getResources().getColor(R.color.mainActivityBackground));
            score_tv.setText("0");
        }
    }
}
